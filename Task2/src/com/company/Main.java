package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.GenericDeclaration;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        int counter = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter num of cities: ");
        int numOfCities = Integer.parseInt(reader.readLine());
        Graph graph = new Graph(numOfCities);
        for(int i = 0; i < numOfCities; i++){
            System.out.println("Enter the name of the city ");
            String name = reader.readLine();
            graph.addVertex(name);
            System.out.println("Enter the number of neighbors ");
            int a = Integer.parseInt(reader.readLine());
            for(int j = 0; j < a; j++){
                System.out.println("Enter index and cost ");
                String s = reader.readLine();
                String[] nums = s.split(" ");
                graph.addEdge(counter, Integer.parseInt(nums[0]) - 1, Integer.parseInt(nums[1]));
            }
            counter++;
        }

        System.out.println("Enter the num of path ");
        int countPaths = Integer.parseInt(reader.readLine());
        for (int i = 0; i < countPaths; i++){
            System.out.println("Enter the source and destination ");
            String citys = reader.readLine();
            String[] cityNames = citys.split(" ");
            int startIndex = 0;
            int endIndex = 0;
            for(Vertex v : graph.vertexList){
                if(v.getLabel().equals(cityNames[0])){
                    startIndex = v.getPos();
                }
                if(v.getLabel().equals(cityNames[1])){
                    endIndex = v.getPos();
                }
            }
            graph.path(startIndex, endIndex);
        }
    }
}
