package com.company;


import java.util.List;
import java.util.ArrayList;

public class Graph {
    private final int MAX_VERTEX;// maximum number of vertices
    private final int INFINITY = 100000000; // this number we have will serve as "infinity"
    List<Vertex> vertexList = new ArrayList<>();// vertex list
    private int relationMatrix[][]; // vertex matrix
    private int countOfVertices; // current number of vertices
    private int countOfVertexInTree; // the number of vertices considered in the tree
    private List<Path> shortestPaths; // shortcut data list
    private int currentVertex; // current vertex
    private int startToCurrent; //distance to currentVertex
    private int startPath;

    public Graph(int numOfCities) {
        if(numOfCities > 10000) throw new IllegalArgumentException();
        MAX_VERTEX = numOfCities;
        relationMatrix = new int[MAX_VERTEX][MAX_VERTEX];// adjacency matrix
        countOfVertices = 0;
        countOfVertexInTree = 0;
        for (int i = 0; i < MAX_VERTEX; i++) {// adjacency matrix to be filled
            for (int k = 0; k < MAX_VERTEX; k++) { // infinite distances
                relationMatrix[i][k] = INFINITY; // default setting
            }
        }
        shortestPaths = new ArrayList<>();// is empty
    }

    public void addVertex(String lab) {// задание новых вершин
        vertexList.add(new Vertex(lab, countOfVertices++));
    }

    public void addEdge(int start, int end, int weight) {
        relationMatrix[start][end] = weight; // setting of the ribs between the vertices, with the weight between them
    }

    public void path(int startPath, int endPath) { // shortcut
        //  setting the data for the start vertex
        this.startPath = startPath;
        int startTree = startPath;
        vertexList.get(startTree).setInTree(true); // inclusion of the first element in the tree
        countOfVertexInTree = 1;

        // filling of short paths for vertices adjacent to the start
        for (int i = 0; i < countOfVertices; i++) {
            int tempDist = relationMatrix[startTree][i];
            Path path = new Path(tempDist);
            path.getParentVertices().add(startPath);// первым родительским элементом, будет всегда стартовая вершина
            shortestPaths.add(path);
        }
        // until all the vertices are in the tree
        // run until the number of vertices in the tree equals the total number of vertices
        while (countOfVertexInTree < countOfVertices) {
            //get the index of the vertex with the smallest distance, from the vertices not yet included in the tree
            int indexMin = getMin();
            // minimum distance of the vertex, from that are not yet in the tree
            int minDist = shortestPaths.get(indexMin).getDistance();

            if (minDist == INFINITY) {
                break;// in the event that only unreachable vertices remain, we exit the cycle
            } else {
                currentVertex = indexMin; // move the currentVert pointer to the current vertex
                startToCurrent = shortestPaths.get(indexMin).getDistance();// specify the distance to the current vertex
            }

            vertexList.get(currentVertex).setInTree(true);  //inclusion of the current vertex in the tree
            countOfVertexInTree++; // increase the vertex counter in the tree
            updateShortestPaths(); // updating the shortest path list
        }

        displayPaths(endPath); // выводим в консоль результаты
        clean();
    }

    public void clean() { // wood peel
        countOfVertexInTree = 0;
        for (int i = 0; i < countOfVertices; i++) {
            vertexList.get(i).setInTree(false);
        }
        shortestPaths.clear();
    }

    private int getMin() {
        int minDist = INFINITY; // "infinite" length is taken as the starting point
        int indexMin = 0;
        for (int i = 0; i < countOfVertices; i++) {
            // if the top is not already in the tree and its distance is less than the old minimum
            if (!vertexList.get(i).isInTree() && shortestPaths.get(i).getDistance() < minDist) {
                minDist = shortestPaths.get(i).getDistance(); // minimum set
                indexMin = i; // updating the vertex index containing the minimum distance
            }
        }
        //returns the index of the vertex with the smallest distance, from the vertices not yet in the tree
        return indexMin;
    }

    private void updateShortestPaths() {
        int vertexIndex = startPath;
        while (vertexIndex < countOfVertices) { // column count
            // if the column vertex is already included in the tree, it is skipped
            if (vertexList.get(vertexIndex).isInTree()) {
                vertexIndex++;
                continue;
            }
            int currentToFringe = relationMatrix[currentVertex][vertexIndex];
            int startToFringe = startToCurrent + currentToFringe;
            int shortPathDistance = shortestPaths.get(vertexIndex).getDistance();

            //currentVertex distance comparison with current vertex distance with vertexIndex index
            if (startToFringe < shortPathDistance) {// еif less, the vertexIndex will have a new shortest path
                //Copy the list of parents of currentVert
                List<Integer> newParents = new ArrayList<>(shortestPaths.get(currentVertex).getParentVertices());
                newParents.add(currentVertex);// set in it and currentVertex as previous
                shortestPaths.get(vertexIndex).setParentVertices(newParents); // We’re protecting a new march
                shortestPaths.get(vertexIndex).setDistance(startToFringe); // Save a new distance
            }
            vertexIndex++;
        }
    }

    private void displayPaths(int n) { // метод для вывода кратчайших путей на экран
        System.out.println(shortestPaths.get(n).getDistance());
    }
}