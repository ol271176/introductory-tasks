package com.company;

import java.util.ArrayList;
import java.util.List;

public class Path { // an object of this class containing distances, previous and completed vertices
    private int distance; // current distance from the starting peak
    private List<Integer> parentVertices; // current vertex parent

    public Path(int distance) {
        this.distance = distance;
        this.parentVertices = new ArrayList<>();
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public List<Integer> getParentVertices() {
        return parentVertices;
    }

    public void setParentVertices(List<Integer> parentVertices) {
        this.parentVertices = parentVertices;
    }
}