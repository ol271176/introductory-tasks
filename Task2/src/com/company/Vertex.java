package com.company;

public class Vertex{
    private String label;
    private boolean isInTree;
    private int pos;

    public Vertex(String label, int pos) {
        this.label = label;
        this.isInTree = false;
        this.pos = pos;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isInTree() {
        return isInTree;
    }

    public void setInTree(boolean inTree) {
        isInTree = inTree;
    }

    public int getPos() {
        return pos;
    }
}
