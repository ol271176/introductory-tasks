import java.math.BigInteger;
import java.util.Arrays;

public class Factorial {
    /**
     * Takes a number and returns sum of digits from factorial.
     * @author ol271176@gmail.com
     * @param n The value of factorial.
     * @return The sum of numbers from factorial.
     */
    public static long getSumOfDigits(int n) throws WrongArgumentException{
        if (n < 0) throw new WrongArgumentException("number is less than 1"); //verify input
        BigInteger factorialNumber = factorial(n);
        //returning sum of digits
        return Arrays.stream(factorialNumber.toString().split("")).mapToInt(Integer::parseInt).sum();
    }

    //calculate factorial
    private static BigInteger factorial(int n) {
        BigInteger result = BigInteger.ONE;
        if (n == 1 || n == 0) {
            return result;
        }
        result = factorial(n-1).multiply(BigInteger.valueOf(n));
        return result;
    }

}


