package com.company;

public class BracesCounter {

    private static int counter = 0;

    public static void checkExpression(String input) {
        //removing redundant symbols
        String nexText = input.replaceAll("[a-zA-z0-9\\s\\w\\+\\-\\/\\*]", "");
        char[] arr = nexText.toCharArray(); //split on characters
        int openedBraces = 0;
        int closedBraces = 0;
        for (char e : arr) {
            switch (e) {
                case '(':
                    openedBraces++;
                    break;
                case ')':
                    closedBraces++;
                    break;
            }
            if (closedBraces > openedBraces) {
                System.out.format("%s - is an invalid braces expression\n", input);
            }
        }
        if(closedBraces == openedBraces) System.out.format("%s - is a correct braces expression\n", input);
    }

    public static void amountOfCorrectBraces(int n) {
        System.out.format("Number of correct braces expressions %d*%d - %d",n, n, printBraces(new char[2 * n], 0, n, 0, 0));
    }

    private static int printBraces(char[] str, int pos, int n, int opened, int closed) {
        if (closed == n) {
            for (char c : str) System.out.print(c);
            System.out.println();
            counter++;
            return 0;
        } else {
            if (opened > closed) {
                str[pos] = ')';
                printBraces(str, pos + 1, n, opened, closed + 1);
            }
            if (opened < n) {
                str[pos] = '(';
                printBraces(str, pos + 1, n, opened + 1, closed);
            }
        }
        return counter;
    }
}
